module.exports = project => (
`const mongoose = require('mongoose')

const UserSchema = new mongoose.Schema({
	email: { 
		type     : String, 
		required : true,
		unique   : true
	},
	password: {
		type     : String, 
		required : true
	},
	admin: {
		type     : Boolean,
		required : true,
		default  : false
	},
	created : { 
		type: Date, 
		default: Date.now 
	}
});

module.exports = mongoose.model('Users', UserSchema)`
)