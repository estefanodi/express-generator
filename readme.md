A generator is tool which will create the structure of your app in few comands.

To build this we've used :

* node readline   https://nodejs.org/api/readline.html
* node filesystem https://nodejs.org/api/fs.html
* javascript 

How to use it :

1) cd express_generator
2) node app.js
3) then you'll be asked for project name , author and description
   (these data will be included in the package.json)
4) cd ${new project name}
5) npm install 
6) nodemon

